from flask import Flask
from app.model.database_config import Config
from flask_sqlalchemy import SQLAlchemy



app = Flask(__name__)
app.config.from_object(Config)
db=SQLAlchemy(app)
db.init_app(app)

