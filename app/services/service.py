from app.model.students_table import Students


class Service():

    #passing values name and email from routes
    @classmethod
    def add_student(cls,name,email):

        #calling a classmethod from students table and passing the values name and email

        new_student= Students.post_stu(name,email)

        #after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return new_student

    # passing request here to getall
    @classmethod
    def getall_student(cls):
        # calling a classmethod from students table

        getall_student = Students.getall_stu()

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return getall_student

    # passing request id here to get values
    @classmethod
    def get_student(cls, id):

        # calling a classmethod and passing id from students table

        getby_studentid = Students.get_stu(id)

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return getby_studentid

    # passing values name and email from routes
    @classmethod
    def put_student(cls,id,name,email):

        # calling a classmethod and passing id,values from students table

        update_student = Students.put_stu(id,name,email)

        #after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return update_student

    @classmethod
    def del_student(cls,id):

        # calling a classmethod and passing id from students table

        delete_student = Students.del_stu(id)

        # after returning and get dict data from students table classmethod here stu returns the data and passing data to routes response

        return delete_student
