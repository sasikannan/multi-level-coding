import unittest
from app.services.service import Service


class GetStudent(unittest.TestCase):

    def test_get_id(self):
        result= Service.get_student(13)
        self.assertEqual("rkrasasasai@gmail.com",result.get("email"))

    def test_post_students(self):
        result=Service.add_student(name="roomilojlkuaru",email="roomiolojlkuaru@gmail.com")
        self.assertEqual("roomiolojlkuaru@gmail.com",result.get("email"))

    def test_getall(self):
        result= Service.getall_student()
        self.assertEqual(not None,result is not None)

    def test_put(self):
        result=Service.put_student(id=13,name="rasi",email="rkrasasasai@gmail.com")
        self.assertEqual("rkrasasasai@gmail.com",result.get("email"))

    def test_del(self):
        result=Service.del_student(13)
        self.assertEqual(True,result)









