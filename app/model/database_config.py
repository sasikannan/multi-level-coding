from os import  path

basedir = path.abspath(path.dirname(__file__))


class Config:

    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:12345@localhost/sasi"
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False