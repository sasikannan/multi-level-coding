from app import db
import app.model.students_table


class Base():
    #converting object to dict

    def stu_dict(self):
        stu_dict = self.__dict__
        stu_dict.pop('_sa_instance_state')
        return stu_dict

    #coberting sqlalchemyobj to dict
    def studict_sqlalchemyobj(self):
        result = {
            "id": self.id,
            "name": self.name,
            "email": self.email
        }
        return result

    #iterate converted object to get all data

    def stu_dict_getall(self):
        stu = {"students": []}
        for i in self:
            stu_dict = Base.stu_dict(i)
            stu["students"].append(stu_dict)
        return stu

    #get students query from database by id

    def get_student(id):
         Students1 = app.model.students_table.Students.query.get(id)
         return Students1

    # getall students query from database by id

    def get_students():
        Students1 = app.model.students_table.Students.query.all()
        return Students1

    #commit query to update

    def update():
        db.session.commit()

    #add and commit query

    def save_to_db_student(self):
        db.session.add(self)
        db.session.commit()
        return self

    #delete and commit query

    def delete_from_db_student(self):
        db.session.delete(self)
        db.session.commit()
        return True










