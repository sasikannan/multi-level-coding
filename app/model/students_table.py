from app import db
from app.model.base import Base


class Students(db.Model):

             ######### creating a table #########

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True)
    email = db.Column(db.String(80), unique=True)

    def __init__(self, name, email):
        self.name = name
        self.email = email

        ######## classmethods to do perform in database and get return value in postman ######

   #got values from services and performing post process in database
    @classmethod
    def post_stu(cls,name,email):
        #assigning passing value as variable

        student1 = Students(name=name,email=email)

        #calling class from base it save and commit the data

        st=Base.save_to_db_student(student1)

        # calling class from base it convert object to dict and return value as dict

        return Base.studict_sqlalchemyobj(st)



    # got values from services and performing get process in database
    @classmethod
    def getall_stu(cls):
        # calling class from base querying all from database

        student1 = Base.get_students()

        # calling class from base it convert object to dict and return value as dict

        return Base.stu_dict_getall(student1)

    # got values from services and performing get process in database
    @classmethod
    def get_stu(cls,id):
        # calling class from base query by id from database

        student1 = Base.get_student(id)

        # calling class from base it convert object to dict and return value as dict

        return Base.stu_dict(student1)

    # got values from services and performing put process in database
    @classmethod
    def put_stu(cls,id,name,email):

        #get id from database to update

        students1=Base.get_student(id)

        #assigning passing data as variable to database

        student1 = Students(name=name, email=email)

        #performing update

        students1.name=name
        students1.email=email

        # calling class from base and commit the data

        Base.update()

        # calling class from base it convert object to dict and return value as dict

        return Base.studict_sqlalchemyobj(student1)

    # got values from services and performing delete process in database
    @classmethod
    def del_stu(cls,id):
        # get id from database to delete

        student1=Base.get_student(id)

        # calling class from base and delete the id

        return Base.delete_from_db_student(student1)

        # calling class from base it convert object to dict and return value as dict


db.create_all()