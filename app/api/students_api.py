from flask import Blueprint,request
import app.services.service

####### Blueprint for api routes #######

site= Blueprint(__name__, "site")

###### api routes url ###########

@site.route('/',methods=["GET"])
def home():
      return "<h1> WELCOME TO STUDENTS PROFILE <h1>"

@site.route("/students1/post",methods=["POST"])
def post():
      #requesting to get json format name and email value

      name = request.json["name"]
      email = request.json["email"]

      #calling a class from services and passing the values name,email json in it

      response = app.services.service.Service.add_student(name,email)

      # got data from services class method

      return response

@site.route("/students1/getall",methods=["GET"])
def getall():
      # calling a class from services to get all values

      response = app.services.service.Service.getall_student()

      # got data from services class method

      return response

@site.route("/students1/get/<id>",methods=["GET"])
def get(id):
      # calling a class from services and passing the id from url which is needed to get

      response = app.services.service.Service.get_student(id)

      # got data from services class method

      return response

@site.route("/students1/put/<id>",methods=["PUT"])
def put(id):
      # requesting to get json format name and email value to change or update the data

      name = request.json["name"]
      email = request.json["email"]

      # calling a class from services and passing the values name,email json in it to update or change values

      response = app.services.service.Service.put_student(id,name,email)

      #got data from services class method

      return response

@site.route("/students1/delete/<id>",methods=["DELETE"])
def delete(id):
      # calling a class from services and passing the id from url which is needed to delete

      response = app.services.service.Service.del_student(id)

      # got data from services class method

      return response





